The purpose of this sample cordova application is to demonstrate the usage of Qualpay Embedded fields using Cordova on NodeJS. This application has been built using light weight libraries to demonstrate Qualpay products and is not meant for production use. 

**Dependencies**

You will need
    
    •	A qualpay sandbox account. Create your sandbox account at https://api-test.qualpay.com
	•	Ensure that you have an api security key and the key has access Payment Gateway API and Embedded Fields API. Refer to https://www.qualpay.com/developer/api/testing#security-keys.
    •	NodeJS 
    •   express & nodemon nodejs libraries
    •	Cordova and dependencies required for mobile support as detailed on https://cordova.apache.org/
    •	Chrome or Firefox browser for mobile testing. The application has been tested on latest version of Chrome and Firefox.
	
**Configuration**

    config/config.json
	•	Update merchant_id to point to your merchant account id.
	•	Udpate security_key to point to your merchant api security_key.
	•	Update any other property as appropriate.
    
    index.html
    •	Update the backend server ip address as appropriate to point to your backend services. Using localhost will not work for android devices.
    
**Code samples**

	•	index.html - Use Embedded fields to capture card data and use it to invoke a sale request.
    •	api/controllers/embeddedController.js - Get transient key for use with embedded fiels
	•	api/controllers/sale.js - Use Payment gateway to make a payment using card id

**To Run**

	•	To start backend server run the following on your terminal.
    nodemon server.js
    
  	•	Browser testing
    Open index.html on browser
    
  	•	Mobile simulator testing, run the following commands to build app and start the android emulator
    cordova build
    cordova emulate android
