let request = require('request');
const config = require('./../../config/config.json');

/**
 * Get embedded transient key.
 * A transient key expires in 12 hours or If a card is successfully verified
 * on embedded fields using this key.
 */
exports.get_transient_key = (req, res) => {
  //Get a transient key for use with embedded fields
  let auth = new Buffer( ":" + config.qualpay_api_security_key).toString("base64");
  let options = {
        host: config.qualpay_api_base_path,
        path: '/platform/embedded',
        uri: config.qualpay_api_base_path + '/platform/embedded',
        method: 'GET',
        headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/json'
        }
    };
    request(options, (error, response, body) => {
    if (error) {
      console.error(error);
      res.status(401).send(error);
    } else {
      let qpResp = JSON.parse(response.body);
      let embeddedResp = {
        message: qpResp.message,
        code: qpResp.code,
        transient_key: qpResp.data.transient_key,
        merchant_id: qpResp.data.merchant_id,
        expiry_time: qpResp.data.expiry_time
      };
      if (qpResp.respCode){
        res.status(401);
      }
      res.send(embeddedResp);
    }
  });
};
