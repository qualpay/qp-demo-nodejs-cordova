let request = require('request');
const config = require('./../../config/config.json');

/**
 * Post a sale request to Payment Gateway
 * Use card id returned from embedded fields in the request
 */
exports.post_payment = (req, res) => {
  let  
  auth = new Buffer( ":" + config.qualpay_api_security_key).toString("base64"),
  body = {
    amt_tran: req.body.amt_tran,
    merchant_id: config.qualpay_merchant_id,
    card_id: req.body.card_id
  };

  let options = {
    method: 'POST',
    uri: config.qualpay_api_base_path + '/pg/sale',
    headers: {
        Authorization: 'Basic ' + auth,
        'Content-Type': 'application/json'
    },
    body: body,
    json: true,
  }

  request(options, (err, response, body) => {
    let qpResponse = response.body;
    if (err) {
      console.error(err.error);
      res.status(401).send({ error : err });
    } else {
      let saleResp = {
        pgId: qpResponse.pg_id,
        respCode: qpResponse.rcode,
        authCode: qpResponse.auth_code,
        authAvsResult: qpResponse.auth_avs_result,
        msg: qpResponse.rmsg
      };
      if (qpResponse.rcode !==  '000'){ //Declines
        res.status(401).send({ 
          code : qpResponse.rcode, 
          pgId: qpResponse.pg_id, 
          message: qpResponse.rmsg });
      } else {  //Success
        res.send({
          pgId: qpResponse.pg_id,
          code: qpResponse.rcode,
          authCode: qpResponse.auth_code,
          authAvsResult: qpResponse.auth_avs_result,
          message: qpResponse.rmsg
        });
      }
    }
  });
};
