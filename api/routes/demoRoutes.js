module.exports = function (app) {
  let embedded = require('../controllers/embeddedController');
  let sale = require('../controllers/sale');
  const path = require('path');

  app.route('/transientkey')
    .get(embedded.get_transient_key);
  
  app.route('/sale')
    .post(sale.post_payment);
  
  app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+'/../../index.html'));
  });
};